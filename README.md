
## how to run the app
mvn clean package && java -jar target/place-data-provider-1.0-SNAPSHOT.jar

### Design
this app has some beans that each does a task.

#### CreatePolygonForPlace 
starts a thread and update the POLYGON column 
for every place in Place table that it reads.
only if the bean is annotated with PostConstruct

#### CreatePlaceJsonFile

reads all the places from db and creates a json file 
only if the bean is annotated with PostConstruct
this will take 8 minutes to fetch all data from table. be patient

#### PlaceTagJsonFileCreator
creates the right place-tag json file from mango_geo that UI needs to show the tree