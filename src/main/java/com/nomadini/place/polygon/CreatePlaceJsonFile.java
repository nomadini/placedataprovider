package com.nomadini.place.polygon;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.Executors;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcOperations;

import com.fasterxml.jackson.databind.ObjectMapper;

public class CreatePlaceJsonFile {
    public static final String FILENAME = "/tmp/all-places-as-json.txt";
    private OutputStreamWriter osw;
    private final JdbcOperations jdbcOperations;
    private static final Logger LOGGER = LoggerFactory.getLogger(CreatePlaceJsonFile.class);
    private ObjectMapper mapper = new ObjectMapper();

    public CreatePlaceJsonFile(JdbcOperations jdbcOperations) {
        this.jdbcOperations = Objects.requireNonNull(jdbcOperations);
    }

    public void execute() throws IOException {
        openFile();
        emptyFile();

        List<PlaceInDb> places =
                jdbcOperations.query(
                        "select ID, POLYGON, DESCR, ADDRESS, CITY, STATE, POSTAL_CODE, RADIUS, LATITUDE, LONGITUDE from PLACE",
                        new Object[]{},
                        (rs, i) -> {
                            PlaceInDb place = new PlaceInDb();
                            place.setId(rs.getInt("ID"));
                            place.setLat(rs.getDouble("LATITUDE"));
                            place.setLon(rs.getDouble("LONGITUDE"));
                            place.setRadius(rs.getDouble("RADIUS"));
                            place.setDescription(rs.getString("DESCR"));
                            place.setAddress(rs.getString("ADDRESS"));
                            place.setCity(rs.getString("CITY"));
                            place.setState(rs.getString("STATE"));
                            place.setPostalCode(rs.getString("POSTAL_CODE"));
                            place.setPolygon(rs.getString("POLYGON"));

                            return place;
                        });
        int i = 0;
        for(PlaceInDb place : places) {
            i++;
            appendAsJsonToFile(place);
            if (i % 10000 == 1) {
                LOGGER.info("wrote {} th place to file", i);
            }
        }

        LOGGER.info("file is ready");
        osw.close();
        System.exit(0);
    }

    private void emptyFile() throws IOException {
        osw.write("");
    }

    private void openFile() throws FileNotFoundException {
        File fout = new File(FILENAME);
        FileOutputStream fos = new FileOutputStream(fout);

        osw = new OutputStreamWriter(fos);
    }

    private void appendAsJsonToFile(PlaceInDb place) throws IOException {
        String placeAsJson = mapper.writeValueAsString(place);
        LOGGER.debug("place to write is {} ", placeAsJson);
        osw.append(placeAsJson);
        osw.append("\n");
    }


    //uncomment this to start the program
    @PostConstruct
    public void start() throws InterruptedException {
        Thread.currentThread().sleep(7000);
        Executors.newSingleThreadExecutor().submit(() -> {
            try {
                execute();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }
}
