package com.nomadini.place.polygon;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.Executors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcOperations;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Polygon;

import javax.annotation.PostConstruct;

public class CreatePolygonForPlace {

    private final JdbcOperations jdbcOperations;
    private static final Logger LOGGER = LoggerFactory.getLogger(CreatePolygonForPlace.class);
    private ObjectMapper mapper = new ObjectMapper();

    private static final String UPDATE_PLACE_POLYGON =
            "UPDATE `mango_geo`.`PLACE` " +
            " SET " +
            " `POLYGON` = ?  " +
            " WHERE ID = ?";

    public CreatePolygonForPlace(JdbcOperations jdbcOperations) {
        this.jdbcOperations = Objects.requireNonNull(jdbcOperations);
    }

    public void execute() throws IOException {
        try {
            Integer numberOfRecords = 0;
            addPolyGonColumn();
            updateBatchOfPolygons(numberOfRecords);
        } catch (RuntimeException e) {
            e.printStackTrace();
        }
        LOGGER.info("done with place polygon update");
        System.exit(0);
    }

    private void addPolyGonColumn() {
        try {
            LOGGER.info("adding polygon column");
            jdbcOperations.update("ALTER TABLE PLACE ADD COLUMN POLYGON varchar(1000);");
        } catch (RuntimeException e) {
            e.printStackTrace();
        }
    }

    private void updateBatchOfPolygons(Integer numberOfRecords) throws IOException {
        LOGGER.info("updateBatchOfPolygons");

        Integer countOfPlaces = jdbcOperations.queryForObject("select count(*) AS COUNT from PLACE",
                (resultSet, i) -> resultSet.getInt("COUNT"));

        for (int offset = 0; offset < countOfPlaces; offset = offset + 10000) {
            List<PlaceInDb> places =
                    jdbcOperations.query("select ID, RADIUS, LATITUDE, LONGITUDE from PLACE " +
                                    "WHERE POLYGON IS NULL LIMIT 10000 OFFSET " + offset,
                            new Object[]{},
                            (rs, j) -> {
                                PlaceInDb place = new PlaceInDb();
                                place.setId(rs.getInt("ID"));
                                place.setLat(rs.getDouble("LATITUDE"));
                                place.setLon(rs.getDouble("LONGITUDE"));
                                place.setRadius(rs.getDouble("RADIUS"));

                                return place;
                            });
            writeInBatches(places, numberOfRecords);
        }

    }

    private void writeInBatches(List<PlaceInDb> places, Integer numberOfRecords) throws IOException {
        int i=0;
        List<PlaceInDb> batch = new ArrayList<>();
        for(PlaceInDb place : places) {
            i++;
            Polygon polygon = GeometryUtil.getRectanglePolygonFromPointAndRadius(place.getLat(), place.getLon(), place.getRadius());

            Coordinate[] allCoordinates = polygon.getCoordinates();
            PlacePolygon pp = new PlacePolygon();
            pp.setType("Polygon");
            List<List<List<Double>>> aa = new ArrayList<>();
            List<List<Double>> ab = new ArrayList<>();

            for (Coordinate cor : allCoordinates) {
                List<Double> ac = new ArrayList<>();
                ac.add(cor.getOrdinate(1));
                ac.add(cor.getOrdinate(0));
                ab.add(ac);
            }
            aa.add(ab);
            pp.setGeometry(aa);

            String polygonDetails = mapper.writeValueAsString(pp);
//            LOGGER.debug("polygon : {}", polygonDetails);
//            {"type":"Polygon","geometry":[[[40.7300990706861,-73.9587392228485],[40.7301655104705,-73.9580410522096],[40.7299521677305,-73.958005928082],[40.7298857958069,-73.9587041885524],[40.730071500879,-73.9587346414405],[40.7300990706861,-73.9587392228485]]]}

            place.setPolygon(polygonDetails);

            batch.add(place);
            numberOfRecords++;
            if (batch.size() > 10000) {
                writeBatch(numberOfRecords, batch);
                batch.clear();
            }
        }

        writeBatch(numberOfRecords, batch);//write the batch for the final time
    }

    private void writeBatch(Integer numberOfRecords, List<PlaceInDb> batch) throws IOException {
        if (batch.isEmpty()) {
            return;
        }
        updatePolygon(batch);
        LOGGER.debug("updated {} th place to db", numberOfRecords);
        LOGGER.info("updating place : {}", batch.get(0));
    }

    private void updatePolygon(List<PlaceInDb> places) {

        jdbcOperations.batchUpdate(UPDATE_PLACE_POLYGON, new BatchPreparedStatementSetter() {

            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                PlaceInDb placeInDb = places.get(i);
                ps.setString(1, placeInDb.getPolygon());
                ps.setInt(2, placeInDb.getId());
            }

            @Override
            public int getBatchSize() {
                return places.size();
            }
        });

    }

    @PostConstruct
    public void start() throws InterruptedException {
        Thread.sleep(7000);
        Executors.newSingleThreadExecutor().submit(new Runnable() {

            @Override
            public void run() {
                try {
                    execute();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
