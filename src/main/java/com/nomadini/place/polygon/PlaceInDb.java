package com.nomadini.place.polygon;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

public class PlaceInDb {
    private int id;
    private double lat;
    private double lon;
    private double radius;
    private String polygon;
    private String description;
    private String address;
    private String city;
    private String state;
    private String postalCode;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public String getPolygon() {
        return polygon;
    }

    public void setPolygon(String polygon) {
        this.polygon = polygon;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PlaceInDb)) return false;
        PlaceInDb placeInDb = (PlaceInDb) o;
        return getId() == placeInDb.getId() &&
                Double.compare(placeInDb.getLat(), getLat()) == 0 &&
                Double.compare(placeInDb.getLon(), getLon()) == 0 &&
                Double.compare(placeInDb.getRadius(), getRadius()) == 0 &&
                Objects.equal(getPolygon(), placeInDb.getPolygon()) &&
                Objects.equal(getDescription(), placeInDb.getDescription()) &&
                Objects.equal(getAddress(), placeInDb.getAddress()) &&
                Objects.equal(getCity(), placeInDb.getCity()) &&
                Objects.equal(getState(), placeInDb.getState()) &&
                Objects.equal(getPostalCode(), placeInDb.getPostalCode());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId(), getLat(), getLon(), getRadius(), getPolygon(), getDescription(), getAddress(), getCity(), getState(), getPostalCode());
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("lat", lat)
                .add("lon", lon)
                .add("radius", radius)
                .add("polygon", polygon)
                .add("description", description)
                .add("address", address)
                .add("city", city)
                .add("state", state)
                .add("postalCode", postalCode)
                .toString();
    }
}
