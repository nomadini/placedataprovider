package com.nomadini.place.polygon;

import java.util.ArrayList;
import java.util.List;

public class PlacePolygon {
    private String type;
    private List<List<List<Double>>> geometry = new ArrayList<>();

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<List<List<Double>>> getGeometry() {
        return geometry;
    }

    public void setGeometry(List<List<List<Double>>> geometry) {
        this.geometry = geometry;
    }
}
