package com.nomadini.placetag;

import java.util.ArrayList;
import java.util.List;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

public class Tag {
    private int id;
    private int parentId;
    private String name;
    private String description;
    private String status;
    private List<Tag> children = new ArrayList<>();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getParentId() {
        return parentId;
    }

    public void setParentId(int parentId) {
        this.parentId = parentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<Tag> getChildren() {
        return children;
    }

    public void setChildren(List<Tag> children) {
        this.children = children;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Tag)) return false;
        Tag tag = (Tag) o;
        return getId() == tag.getId() &&
                getParentId() == tag.getParentId() &&
                Objects.equal(getName(), tag.getName()) &&
                Objects.equal(getDescription(), tag.getDescription()) &&
                Objects.equal(getStatus(), tag.getStatus()) &&
                Objects.equal(getChildren(), tag.getChildren());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId(), getParentId(), getName(), getDescription(), getStatus(), getChildren());
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("parentId", parentId)
                .add("name", name)
                .add("description", description)
                .add("status", status)
                .add("children", children)
                .toString();
    }
}
