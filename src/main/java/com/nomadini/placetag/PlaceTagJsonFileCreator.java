package com.nomadini.placetag;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.Executors;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.jdbc.core.RowMapper;

import com.fasterxml.jackson.databind.ObjectMapper;

import com.nomadini.place.polygon.CreatePlaceJsonFile;

/*
creates a json that is suitable for UI to draw a tree in Edit/Add TargetGroup pages.
 */
public class PlaceTagJsonFileCreator {
    public static final String FILENAME = "/tmp/place-tags.txt";
    private OutputStreamWriter osw;
    private final JdbcOperations jdbcOperations;
    private static final Logger LOGGER = LoggerFactory.getLogger(CreatePlaceJsonFile.class);
    private ObjectMapper mapper = new ObjectMapper();

    public PlaceTagJsonFileCreator(JdbcOperations jdbcOperations) {
        this.jdbcOperations = Objects.requireNonNull(jdbcOperations);
    }

    public void execute() throws IOException {
        openFile();
        emptyFile();

        List<Tag> parentTags =
                jdbcOperations.query(
                        "select ID, NAME, DESCR, PARENT_ID, STATUS from TAG WHERE PARENT_ID = 0 AND STATUS = 'Public'",
                        new Object[]{},
                        getTagRowMapper());

        TagContainer tagContainer = new TagContainer();
        for (Tag tag : parentTags) {
            //this is a parent node, we get the children and write it to file
            tag.setChildren(fetchChildren(tag.getId()));

        }
        tagContainer.setPlaceTags(parentTags);
        appendAsJsonToFile(tagContainer);


        LOGGER.info("file is ready");
        osw.close();
        System.exit(0);
    }

    private List<Tag> fetchChildren(int parentId) {
        return jdbcOperations.query(
                        "select ID, NAME, DESCR, PARENT_ID, STATUS from TAG WHERE PARENT_ID=?",
                        new Object[]{parentId},
                getTagRowMapper());
    }

    private RowMapper<Tag> getTagRowMapper() {
        return (rs, i) -> {
            Tag tag = new Tag();
            tag.setId(rs.getInt("ID"));
            tag.setParentId(rs.getInt("PARENT_ID"));
            tag.setName(rs.getString("NAME"));
            tag.setDescription(rs.getString("DESCR"));
            tag.setStatus(rs.getString("STATUS"));

            return tag;
        };
    }

    private void emptyFile() throws IOException {
        osw.write("");
    }

    private void openFile() throws FileNotFoundException {
        File fout = new File(FILENAME);
        FileOutputStream fos = new FileOutputStream(fout);

        osw = new OutputStreamWriter(fos);
    }

    private void appendAsJsonToFile(TagContainer tag) throws IOException {
        String tagAsJson = mapper.writeValueAsString(tag);
        LOGGER.debug("tag to write is {} ", tagAsJson);
        osw.append(tagAsJson);
    }


    //uncomment this to start the program
    @PostConstruct
    public void start() throws InterruptedException {
        Thread.currentThread().sleep(7000);
        Executors.newSingleThreadExecutor().submit(() -> {
            try {
                execute();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }
}
