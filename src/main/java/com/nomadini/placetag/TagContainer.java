package com.nomadini.placetag;

import java.util.ArrayList;
import java.util.List;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

public class TagContainer {
    private List<Tag> placeTags = new ArrayList<>();

    public List<Tag> getPlaceTags() {
        return placeTags;
    }

    public void setPlaceTags(List<Tag> placeTags) {
        this.placeTags = placeTags;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TagContainer)) return false;
        TagContainer that = (TagContainer) o;
        return Objects.equal(getPlaceTags(), that.getPlaceTags());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getPlaceTags());
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("placeTags", placeTags)
                .toString();
    }
}
