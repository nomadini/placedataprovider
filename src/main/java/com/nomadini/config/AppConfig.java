package com.nomadini.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.nomadini.place.polygon.CreatePlaceJsonFile;
import com.nomadini.place.polygon.CreatePolygonForPlace;
import com.nomadini.placetag.PlaceTagJsonFileCreator;

@Configuration
public class AppConfig extends WebMvcConfigurerAdapter {

    @Autowired
    private DataSource dataSource;

    //Enable if you need this job
    @Bean
    public CreatePolygonForPlace createPolygonForPlace() {
        return new CreatePolygonForPlace(new JdbcTemplate(dataSource));
    }

    //Enable if you need this job
//    @Bean
    public CreatePlaceJsonFile createPlaceJsonFile() {
        return new CreatePlaceJsonFile(new JdbcTemplate(dataSource));
    }

//    @Bean
    public PlaceTagJsonFileCreator placeTagJsonFileCreator() {
        return new PlaceTagJsonFileCreator(new JdbcTemplate(dataSource));
    }

}
