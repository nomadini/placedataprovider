package com.nomadini.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

@Configuration
public class DbConfig {

    @Value("${dataSource.url}")
    private String dataSourceUrl;

    @Value("${dataSource.username}")
    private String username;

    @Value("${dataSource.password}")
    private String password;

    @Value("${dataSource.cachePrepStmts}")
    private boolean cachePrepStmts;

    @Value("${dataSource.prepStmtCacheSize}")
    private Integer prepStmtCacheSize;

    @Value("${dataSource.prepStmtCacheSqlLimit}")
    private Integer prepStmtCacheSqlLimit;

    @Bean
    public DataSource dataSource() {
        HikariConfig config = new HikariConfig();

        config.setJdbcUrl(dataSourceUrl);
        config.setUsername(username);
        config.setPassword(password);

        config.addDataSourceProperty("cachePrepStmts", cachePrepStmts);
        config.addDataSourceProperty("prepStmtCacheSize", prepStmtCacheSize);
        config.addDataSourceProperty("prepStmtCacheSqlLimit", prepStmtCacheSqlLimit);

        return new HikariDataSource(config);
    }
}
